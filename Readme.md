## TIFF to PDF converter

#### Usage:

```
$ python tif_to_pdf.py -h

usage: tif_to_pdf.py [options] source_file(s)

options:
  -h         Help
  -p         Max page to convert. Default is no limit
  -o         output directory. Default is current directory

Examples:
	python tif_to_pdf.py -o output images/CCTF1.TIF
	python tif_to_pdf.py -o output images/*.TIF
```
