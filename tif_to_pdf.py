import sys
import os
import getopt
from PIL import Image
from reportlab.pdfgen import canvas
import reportlab.lib.pagesizes as pdf_sizes

def TIFF2PDF(tif_filepath, output_dir='.', max_pages=None):
    '''
    Convert a TIFF Image into a PDF.
    max_pages: Break after a number of pages. Set to None to have no limit.
    '''
    filename = os.path.basename(tif_filepath)
    filename, file_extension = os.path.splitext(filename)

    tiff_img = Image.open(tif_filepath)

    # Get tiff dimensions from exiff data. The values are swapped for some reason.
    height, width = tiff_img.tag[0x101][0], tiff_img.tag[0x100][0]

    # Create our output PDF
    out_pdf_io = os.path.join(output_dir, filename + '.pdf')
    c = canvas.Canvas(out_pdf_io, pagesize=pdf_sizes.letter)

    # The PDF Size
    pdf_width, pdf_height = pdf_sizes.letter

    # Iterate through the pages
    page = 0
    while True:
        try:
                tiff_img.seek(page)
        except EOFError:
                break
        # Stretch the TIFF image to the full page of the PDF
        if pdf_width * height / width <= pdf_height:
            # Stretch wide
            c.drawInlineImage(tiff_img, 0, 0, pdf_width, pdf_width * height / width)
        else:
            # Stretch long
            c.drawInlineImage(tiff_img, 0, 0, pdf_height * width / height, pdf_height)
        c.showPage()
        if max_pages and page > max_pages:
            print("Too many pages, breaking early")
            break
        page += 1

    print("Saving tiff image to: %s" % out_pdf_io)
    c.save()

def help():
    program_name = os.path.basename(sys.argv[0])
    sys.stdout.write('usage: %s [options] source_file(s)\n' % (program_name))
    sys.stdout.write('\noptions:\n')
    sys.stdout.write('  -h         Help\n')
    sys.stdout.write('  -p         Max page to convert. Default is no limit\n')
    sys.stdout.write('  -o         output directory. Default is current directory \n')
    sys.stdout.write('\nExamples:\n')
    sys.stdout.write('tif_to_pdf.py -o ouput images/CCTF1.TIF\n')
    sys.stdout.write('tif_to_pdf.py -o ouput images/*.TIF\n')

def main():
    if len(sys.argv) == 1:
        help()

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'o:hp:')
    except getopt.error:
        help()
        return False

    output_dir = '.'
    max_pages = None
    for opt, value in opts:
        if opt == '-o':
            output_dir = value
        if opt == '-p':
            max_pages = int(value)
        if opt == '-h':
            help()
            return True

    for file in args:
        TIFF2PDF(file, output_dir=output_dir, max_pages=max_pages)
    return True

if __name__ == '__main__':
    main()